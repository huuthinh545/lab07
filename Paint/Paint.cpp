// Paint.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Paint.h"


#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

////////////////////////
bool isDrawing = FALSE;
int currentShape;
int currentX;
int currentY;
int lastX;
int lastY;

vector<CShape *> Shapes;
////////////////////////

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_PAINT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_PAINT));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_PAINT));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_PAINT);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE: {
		HMENU hMenu = GetMenu(hWnd);
		currentShape = IDD_LINE;
		CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);
		break;
	}

	case WM_LBUTTONDOWN: {
		currentX = GET_X_LPARAM(lParam);
		currentY = GET_Y_LPARAM(lParam);

		if (!isDrawing)
			isDrawing = true;
		break;
	}

	case WM_MOUSEMOVE: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		if (isDrawing) {
			lastX = x;
			lastY = y;
			if ((wParam & MK_SHIFT) && currentShape != IDD_LINE)
			{
				int temp = abs(currentX - lastX);
				if (lastY >= currentY)
					lastY = currentY + temp;
				else
					lastY = currentY - temp;
			}
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	}

	case WM_LBUTTONUP: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);

		if (isDrawing) {
			lastX = x;
			lastY = y;
			isDrawing = false;

			if ((wParam & MK_SHIFT) && currentShape != IDD_LINE)
			{
				int temp = abs(currentX - lastX);
				if (lastY >= currentY)
					lastY = currentY + temp;
				else
					lastY = currentY - temp;
			}

			switch (currentShape)
			{
			case IDD_LINE: {
				CLine *line = new CLine;
				line->SetData(currentX, currentY, lastX, lastY);
				Shapes.push_back(line);
			}
				break;
			case IDD_RECTANGLE: {
				CRectangle* rec = new CRectangle;
				rec->SetData(currentX, currentY, lastX, lastY);
				Shapes.push_back(rec);
			}
				break;
			case IDD_ELLIPSE: {
				CEllipse* ellip = new CEllipse;
				ellip->SetData(currentX, currentY, lastX, lastY);
				Shapes.push_back(ellip);
			}
				break;
			}
		}
	}

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
			HMENU hMenu = GetMenu(hWnd);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
			case ID_DRAW_LINE:
				currentShape = IDD_LINE;
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_CHECKED);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
				break;
			case ID_DRAW_RECTANGLE:
				currentShape = IDD_RECTANGLE;
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_CHECKED);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_UNCHECKED);
				break;
			case ID_DRAW_ELLIPSE:
				currentShape = IDD_ELLIPSE;
				CheckMenuItem(hMenu, ID_DRAW_LINE, MF_UNCHECKED);
				CheckMenuItem(hMenu, ID_DRAW_RECTANGLE, MF_UNCHECKED);
				CheckMenuItem(hMenu, ID_DRAW_ELLIPSE, MF_CHECKED);
				break;
			default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
			int n = Shapes.size();
			for (int i = 0; i < n; i++) {
				Shapes[i]->Draw(hdc);
			}

			if (isDrawing)
			{
				switch (currentShape)
				{
				case IDD_LINE:
					MoveToEx(hdc, currentX, currentY, NULL);
					LineTo(hdc, lastX, lastY);
					break;
				case IDD_RECTANGLE:
					Rectangle(hdc, currentX, currentY, lastX, lastY);
					break;
				case IDD_ELLIPSE:
					Ellipse(hdc, currentX, currentY, lastX, lastY);
					break;
				default:
					break;
				}
			}
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}