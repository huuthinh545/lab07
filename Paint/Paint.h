#pragma once

#include "resource.h"
#include <windowsx.h>
#include <vector>

using namespace std;

#define IDD_LINE 1
#define IDD_RECTANGLE 2
#define IDD_ELLIPSE 3

class CShape {
public:
		int x1;
		int y1;
		int x2;
		int y2;

		virtual void Draw(HDC hdc) = 0;
		virtual CShape* Create() = 0;
		virtual void SetData(int a, int b, int c, int d) = 0;
};

class CLine : public CShape {
public:
	void Draw(HDC hdc) {
		MoveToEx(hdc, x1, y1, NULL);
		LineTo(hdc, x2, y2);
	}

	CShape* Create() {
		return new CLine;
	}

	void SetData(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
};

class CRectangle : public CShape {
public:
	void Draw(HDC hdc) {
		Rectangle(hdc, x1, y1, x2, y2);
	}

	CShape* Create() {
		return new CRectangle;
	}

	void SetData(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
};

class CEllipse : public CShape {
public:
	void Draw(HDC hdc) {
		Ellipse(hdc, x1, y1, x2, y2);
	}

	CShape* Create() {
		return new CEllipse;
	}

	void SetData(int a, int b, int c, int d) {
		x1 = a;
		y1 = b;
		x2 = c;
		y2 = d;
	}
};